## Capitudo 2

  1. O que o SDK faz
    - O codigo gerado e relocavel na forma de biblioteca
    - Mencionar que ele abstrai operacoes REST por traz de um conjunto de chamadas active record
    - Porque preferimos REST/JSON
    - Explicar a opcao pelo software livre
    - Explicar porque optamos por ter um esquema
      - Explicar que o esquema deve ser bom para a leitura de um ser humano e "dane-se" o codigo gerado
    - Talvez historias de usuarios

  2. O que o SDK nao faz
    - Nao gera uma API. Explicar que ele expressa uma API (REST) existente
    - Listar os retornos HTTP que nao sao suportados
    - Explicar que nao daremos suporte a XML
    - Quais as limitacoes do esquema

  3. Prospostas futuras
    - Aliases, usos avancados, etc

## Capitulo 3

  1. Escolha da plataforma

  2. Escolha do Active Record

  3. Especificacao do esquema

  4. Diagramas de classe / sequencia / interacao / packages

  5. Padroes de projeto usados

  6. Organizacao dos modulos da API

## Capitulo 4

  1. Tirar duvidas com Anderson
