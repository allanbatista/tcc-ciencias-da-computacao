    API    Application Programming Interface. Em português: Interface de Programação de Aplicativos.  
    REST    Representational State Transfer. Em português: Transferência de Estado Representacional.  
    HTTP    Hypertext Transfer Protocol. Em português: Protocolo de Transferência de Hipertexto.  
    JSON    Javascript Object Notation. Em português: Notação de Objeto Javascript.  
    XML    Extensible Markup Language. Em português: Linguagem de Marcação Extensível.  
    ECMA    European Computer Manufacturers Association. Em português: Associação Européia de Fabricantes de Computadores.  
    UML    Unified Modeling Language. Em português: Linguagem de Modelagem Unificada.  
    PHP    PHP: Hypertext Processor. Em português: PHP: Processador de Hipertexto.  
    SGBD	    Sistema Gerenciador de Bases de Dados.  
    SOAP    Simple Object Access Protocol, em português Protocolo Simples de Acesso à Objetos.
    RPC    Remote Procedure Call, em português Chamada Remota de Procedimento.
    DML    Data Manipulation Language, em português Linguagem de Manipulação de Dados. 