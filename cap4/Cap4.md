# Aplicação Prática e Conclusão

Nesta etapa, executamos uma demonstração do uso do ActiveRest para acessar recursos REST disponíveis na rede, fazendo contraste com a forma tradicional, na qual são usadas API's voltadas para a comunicação HTTP. Definimos como critério de sucesso em concordância com a hipótese que queremos provar, que o código escrito tem menos linhas e maior clareza semântica quando ActiveRest é empregado.

## Experimento

Acessamos a API de uma empresa chamada SkyHub, que oferece como produto um sistema de "marketplace". "Os Marketplaces são sucesso nos Estados Unidos e começam a ganhar força no Brasil. Funciona assim: lojas de todos os tamanhos expõem seus produtos em grandes e-commerces, que cobram uma comissão sobre as vendas." (SkyHub 2016). Essa empresa oferece uma API REST para controle de loja. A tarefa proposta neste experimento consiste em carregar um "product", atualizar os atributos "name" e "description", executar uma operação de negócio definida na classe de domínio e depois salvar as alterações.

### Chamada usando a biblioteca "Faraday"

Em um processo no qual se usa, por exemplo, a biblioteca "Faraday" para consultar um recurso da API REST, foi necessário escrever código para: definir a classe de domínio; executar uma consulta HTTP (incluindo tratamento de erro); carregar uma instância de uma classe de domínio equivalente com os dados fornecidos no corpo da resposta; ativar o comportamento de negócio na instância da classe de dominio; transformar de volta a classe de domínio em uma requisição HTTP para salvar o recurso; e realizar uma segunda chamada HTTP para atualizar o recurso. Essas atividades de codificação deverão ser realizadas para _todos_ os recursos REST que se deseja acessar, o que significa que será necessário escrever código especializado em cada classe, para executar as transformações e fazer as chamadas HTTP de consulta e salvamento, junto com os tratamentos de erro.

    class Product
      attr_accessor :images, :specifications
      attr_reader   :id, :name, :description, :status,
                    :brand, :ean, :nbm, :qty, :price ,
                    :promocional_price, :cost, :weight,
                    :height, :width, :length

      def initialize options = {}
        id = options.fetch(:id, nil)
        name = options.fetch(:name, nil)
        description = options.fetch(:description, nil)
        status = options.fetch(:status, 'enabled')
        brand = options.fetch(:brand, nil)
        ean = options.fetch(:ean, nil)
        nbm = options.fetch(:nbm, nil)
        qty = options.fetch(:qty, 0)
        price = options.fetch(:price, nil.0)
        promotional_price = options.fetch(:promotional_price, 0.0)
        cost = options.fetch(:cost, nil)
        weight = options.fetch(:weight, 0.0)
        height = options.fetch(:height, 0.0)
        length = options.fetch(:length, 0.0)
        width = options.fetch(:width, 0.0)
        images = options.fetch(:images, [])
        specifications = options.fetch(:specifications, [])        
      end

      def id= value
        @id = value.to_s
      end

      def name= value
        @name = value.to_s
      end

      def description= value
        @description = value.to_s
      end

      def status= value
        @status = value.to_s
      end

      def brand= value
        @brand = value.to_s
      end

      def ean= value
        @ean = value.to_s
      end

      def nbm= value
        @nbm = value.to_s
      end

      def qty= value
        @qty = value.to_i
      end

      def brand= value
        @brand = value.to_s
      end

      def weight= value
        @weight = value.to_f
      end

      def height= value
        @height = value.to_f
      end

      def length= value
        @length = value.to_f
      end

      def width= value
        @width = value.to_f
      end

      def persist!
        @persist = true
      end

      def persisted?
        @persist
      end

      def self.find id
        response = connection.get("/products/#{sku}")

        if response.status >= 500
          raise StandardError.new('Error no servidor')
        elsif response.status == 404
          nil
        else
          parsed = JSON.parse(response.body)

          Product.from_remote(parsed)
        end
      end

      def self.each page, per_page, &block
        response = connection.get("/products", { page: page, per_page: per_page })

        products = from_remotes( JSON.parse(response.body) )

        products.each do |product|
          block.call(product)
        end

        each(page + 1, per_page, &block) if products.size >= per_page
      end

      def save
        success = persisted? ? update : create

        persist! if success

        success
      end

      def update
        response = self.class.update(sku, self.to_remote)

        response.status == 204
      end

      def create
        response = self.class.create(self.to_remote)

        response.status == 201
      end

      def self.update sku, remote
        connection.put("/products/#{sku}", remote.to_json)
      end

      def self.create remote
        connection.post("/products", remote.to_json)
      end

      def self.from_remote parsed
        product = Product.new
        product.id = parsed['sku']
        product.name = parsed['name']
        product.description = parsed['description']
        product.status = parsed['status']
        product.brand = parsed['brand']
        product.ean = parsed['ean']
        product.nbm = parsed['nbm']
        product.qty = parsed['qty']
        product.price = parsed['price']
        product.promocional_price = parsed['promocional_price']
        product.cost = parsed['cost']
        product.weight = parsed['weight']
        product.height = parsed['height']
        product.width = parsed['width']
        product.length = parsed['length']
        product.images = parsed['images']
        product.specifications = parsed['specifications']
        product.persist!
        product
      end

      def self.from_remotes list
        list['products'].map do |product|
          from_remote(product)
        end
      end

      def to_remote
        {
          'product' => {
            'sku' => id,
            'name' => name,
            'description' => description,
            'status' => status,
            'brand' => brand,
            'ean' => ean,
            'nbm' => nbm,
            'qty' => qty,
            'price' => price,
            'promocional_price' => promocional_price,
            'cost' => cost,
            'weight' => weight,
            'height' => height,
            'width' => width,
            'length' => length,
            'images' => images,
            'specifications' => specifications
          }
        }
      end

      def self.connection
        @connection ||= Faraday.new('https://in.skyhub.com.br') do |conn|
          connection.headers['X-User-Email'] = 'activerest@allanbatista.com.br'
          connection.headers['X-User-Token'] = 'iGzpAGGzJsfGrZ6Mhb88'
          connection.headers['Content-Type'] = 'application/json'
          connection.headers['Accept']       = 'application/json'
        end
      end
      
      def some_business_process
      	  # Important business code goes here
      end
    end
    
    product = SkyRuby.Product.find(123)
    product.name = "Outdated product"
    product.description = "Tedious repetitive error prone typing"
    produce.some_business_process()
    product.update()

### Chamada usando "ActiveRest"

Com active REST, escrevemos linhas de código para: estender o mecanismo de autenticação para complementar a requisição HTTP com um cabeçalho requerido pelo provedor do recurso; estender o módulo de conexão para configurar informações básicas HTTP e vincular a classe de autenticação; definir a classe de domínio; atualizar suas informações; chamar o comportamento de negócio e salvar as modificações.

	module Skyruby
    	class Authentication < ActiveRest::Authentication
            def authenticate! path = nil, params = nil, headers = nil
                connection.headers['X-User-Email'] = 'activerest@allanbatista.com.br'
                connection.headers['X-User-Token'] = 'iGzpAGGzJsfGrZ6Mhb88'
                true
            end
        end

        module Connection
            extend ActiveRest::Connection

            authentication Authentication

            host "in.skyhub.com.br"
            protocol 'https'
            port 443

            headers({ "Content-Type" => "application/json", "Accept" => "application/json" })
        end

        class Product
            include ActiveRest::Model

            connection Connection
            resources '/products', options: { offset: 'page', limit: 'per_page' }
            route :update , "/products/:id", { method: 'put' , success: 204 }

            field :id, type: String, remote_name: 'sku'
            field :name, type: String
            field :description, type: String
            field :status, type: String, default: 'enabled'
            field :brand , type: String
            field :ean, type: String
            field :nbm, type: String
            field :qty, type: Integer, default: 0
            field :price, type: 0.0
            field :promotional_price, type: Float, default: 0.0
            field :cost, type: Integer
            field :weight, type: Float, default: 0.0
            field :height, type: Float, default: 0.0
            field :length, type: Float, default: 0.0
            field :width , type: Float, default: 0.0
            field :images, type: Array, default: []
            field :specifications, type: Array, default: []

            def self.parse action, body
                if action.to_s == 'list'
                    parsed = JSON.parse(body)
                    parsed['products']
                elsif ['create', 'update'].include?(action.to_s)
                	{}
                else
                	JSON.parse(body)
                end
            end

            def some_business_process
            	# Business process code goes here
            end
        end
    end

    product = SkyRuby.Product.get(123)
    product.name = "Novel product"
    product.description = "Flabbergasting"
    produce.some_business_process()
    product.update()

### Análise dos Resultados

O código construído usando ActiveRest, utiliza 67 linhas para definição da classe de domínio, conexão e modo customizado de autenticação. Todos os métodos necessários para serialização/desserialização de dados e controle de erros padrão do protocolo HTTP são providos por "ActiveRest::Model", bem como o comportamento necessário para comunicação HTTP. Já o código que utiliza a biblioteca "Faraday" utiliza 206 linhas de código para atingir os mesmos resultados. Nos dois exemplos, outras 5 linhas, carregam o "product" identificado por 123, resultando numa instância que é atualizada, tem uma operação de negócio invocada e depois é salva.

Todos os atributos que fazem parte da classe de domínio escrita usando "ActiveRest", são identificados usando a definição semântica de tipos proposta, trazendo para uma declaração de poucas palavras com muito menos elementos para interferir na interpretação do texto. Por exemplo, na declaração abaixo, é evidente o tipo do dado armazenado no atributo e o quê acontece quando ele não é encontrado na resposta retornada pela consulta HTTP.

			field :status, type: String, default: 'enabled'

Na implementação que usa "Faraday", o código de validação também deve ser escrito para cada campo e o valor atribuído em caso de omissão deve ser identificado a partir da leitura de um trecho de código que contém uma condição lógica.

# Conclusão

Durante todo o processo de desenvolvimento e experimentação, chegamos a conclusão de que foi criado algo realmente útil e reaproveitável. O esforço necessário para construir uma interface de comunicação com uma API REST foi reduzido drasticamente, pois o framework passa a fazer o trabalho por de baixo dos panos, facilitando a vida do desenvolvedor, reduzindo custos e acelerando o processo de desenvolvimento.

Desde o inicio das conversas sobre o projeto, decidimos adotar um padrão de trabalho Ágil (metodologia de gerenciamento de projetos) o que nos ajudou muito e permitiu a conclusão deste trabalho com sucesso.

Vale notar também, que no inicio do projeto, existia uma visão totalmente ofuscada do real problema, pois queríamos construir um “Gerador de SDK” inclusive, construímos um “schema” (modelo) para poder iniciar o projeto de desenvolvimento do gerador. Mas isso era ruim, porque obrigava todas as APIs a seguirem o mesmo padrão e nem sempre é possível, pois não podemos alterar a API de um fornecedor.