# TCC Ciencias da Computacao

- [Capítulo 1](cap1/Cap1.md): Introdução ao problema
- [Capítulo 2](cap2/Cap2.md): Definição das tecnologias que serão utilizadas
- [Capítulo 3](cap3/Cap3.md): Desenvolvimento do Projeto
- [Capítulo 4](cap4/Cap4.md): Conclusão

## Resumo

Sobre a criação de um Framework para atuar como uma camada de abstração para APIs REST de forma a permitir o acesso aos seus recursos como se fossem objetos e classes Ruby, com uma interface inspirada no ActiveRecord e no MongoId. O projeto tem código fonte aberto e ficará disponível no Github: [https://github.com/allanbatista/active-rest](https://github.com/allanbatista/active-rest).

## Abstract

On the construction of a framework inspired by ActiveRecord and Mongoi that provides an object oriented abstraction layer for REST API’s, exposing its resources as Ruby objects. It is Open Source and available at Github: [https://github.com/allanbatista/active-rest](https://github.com/allanbatista/active-rest).