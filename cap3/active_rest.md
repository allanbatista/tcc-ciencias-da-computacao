## ActiveRest

    module ActiveRest
    	attr_accessor :environment

    	class Model
        	connection ConnectionClazz
        	base_path "/"
            default_parser :json

            list nil, headers: {}
            find ":atribute_id"

            field :name, type: String, default: nil
        end

        module ModelParser
        	module JSONParser
            end

            module XMLParser
            end

            module CSVParser
            end
        end

        module ModelField
        	module Types
                module String
                end

                module Integer
                end

                module Float
                end

                module Hash
                end

                module Boolean
                end

                module Array
                end
            end

            module Validates
				module Presence
                end

                module MaxLength
                end

                module MinLength
                end

                module MaxSize
                end

                module MinSize
                end
            end
        end

        module ModelAssociation
        	module BelongsTo
            end

            module HasMany
            end

            module HasManyLocation
            end
        end

        class Proxy
        end

        class Interator
        end

        class Connection
<<<<<<< HEAD
        	attr_reader :host, :port, :connector, :authentication
=======
        	attr_reader :host, :port, :connection, :authentication
>>>>>>> 053785b354b14527982bcca7f6b3170efd04a913

            host 'host'
            port 80
            headers {}
            authentication AuthenticationClazz

            def get path, params, headers
            	@authentication.auth(self)
            end

            def post path, body, headers
            	@authentication.auth(self)
            end

            def delete path, params, headers
            	@authentication.auth(self)
            end

            def put path, body, headers
            	@authentication.auth(self)
            end

            def patch path, body, headers
            	@authentication.auth(self)
            end

            def connector
            end
        end

        class ConnectionTest < ConnectionClazz
			def connector
            end
        end

        module Authentication
        	def self.auth connection
<<<<<<< HEAD

=======
>>>>>>> 053785b354b14527982bcca7f6b3170efd04a913
            end
        end
    end