# Desenvolvimento do Projeto

O sistema foi desenvolvido usando Ruby e o design interno da API foi dividido em duas camadas: comunicação e modelo. Além disso, os recursos de meta-programação de Ruby foram usados extensivamente para construir a API.

Nos diagramas UML, usamos classes com o estereótipo "Module" para expressar a estrutura de mesmo nome no Ruby. Essa estratégia de representação foi usada, pois muito embora a UML não cubra o conceito de módulo proposto pela linguagem, os módulos em Ruby podem ter comportamentos e atributos da mesma forma que classes em outras linguagens. Também podem conter outros módulos, permitindo a construção de uma estrutura de pacotes.

Para estruturar fisicamente o projeto, utilizamos o padrão sugerido para a construção de "Gems" [(Gems 2016)][2] que são bibliotecas reusáveis da linguagem Ruby.

## Adoção do Ruby com meta-programação

É uma linguagem amplamente difundida na web, muito em função do framework "Ruby on Rails", que teve grande crescimento logo no início de seu lançamento e constituiu uma base de usuários significativa nas suas quase duas décadas de vida [(Yegulalp 2015)][1], fornecendo uma API nativa de mapeamento objeto-relacional também baseada em ActiveRecord para acesso a bases de dados. Desenvolvedores que usam "Ruby on Rails" provavelmente já estão familiarizados com essa API. É esperado que, nesses termos, a adoção da nossa API ocorra de forma natural, já que empregamos funcionalidades similares. Usamos os recursos de meta-programação de Ruby, junto com a API "ActiveSupport" para definir o comportamento padrão das domain classes que os desenvolvedores escreverão.

Vale notar que Ruby é uma linguagem muito poderosa e utilizada também fora da Web onde começou sua "carreira" mas ganhou destaque na Web pela sua extensa biblioteca e de facil utilização de Network. É uma linguagem interpretada e que permite modificar o código escrito em tempo de execução. Diferente de outras linguagens como C++, Java, C#, PHP, nas quais o código que será executado é aquele que passará pelo processo de compilação.

Meta programação é uma forma diferente de como construir um programa, pois a ideia muda: paramos de programar pensando nos dados e processos e passamos a programar para o código. Esta é uma ideia um pouco confusa, pois muda o paradigma tradicional de programação. É com esse recurso, que criamos os elementos necessários para substituir tarefas repetitivas e ou tediosas que fazem parte do processo de definição das classes de domínio, como por exemplo, a definição dos atributos de cada classe.

Vamos ilustrar a idéia com um exemplo de dentro da própria implementação do biblioteca do ActiveRest:

    class User
        include ActiveRest::Model

        field :id  , type: Integer
        field :name, type: String, default: 'not_defined', remote_name: 'complete_name'

        route :create, '/users', method: 'post', data_type: 'json'
    end

O módulo "ActiveRest::Model" implementa o método "field" que pode ser utilizado dentro da construção da classe usuário, abstraindo a necessidade de criação de métodos acessores, validação, tradução do nome remoto, tipo e qual o valor padrão.

    user = User.new
    user.name #=> 'not_defined'
    user.name = 'Allan'
    user.name #=> 'Allan'
    user.to_remote #=> { 'complete_name' => 'Allan' }
    user.save #=> true

    outher_user = User.find(123)
    outher_user.name #=> "Leonardo Teixeira"

Como seria escrever esta mesma classe sem meta-programação:

    class User < ActiveRest::Model
        attr_accessor :id, :name

        def initialize
             @name = 'not_defined'
        end

        def to_remote
            {
                'id' => @id,
                'complete_name' => @name
            }
        end

        def parse response
            parsed = JSON.parse(response.body)
            @id    = parsed['id']
            @name  = parsed['complete_name']
        end

        def save
            if id.nil?
                response = connection.connector.post('/users', self.to_remote, { 'Content-Type' => 'application/json' })
                parse(response)
            end
        end
    end

Como podem ver, é clara a vantagem de utilização de meta-programação para reduzir a quantidade de código escrito.

## Arquitetura em camadas

Para permitir o crescimento do software (ActiveRest) de forma progressiva e que possam ser construídos módulos separados para intergrar a biblioteca, distribuímos as responsabilidades em camadas.

![](images/camadas.png)

### Camada de Comunicação

É formada basicamente pela classe "ActiveRest::Connection" que orquestra as operações básicas de HTTP (get, put, post, patch e delete), usando "ActiveRest::Authentication" para configurar o cabeçalho da requisição com as variáveis que fazem parte do esquema de autenticação empregado pela API que se deseja abstrair. Mantém referencia para um objeto que implemente a mesma "interface" de "Faraday::Connector", classe que implementa de fato os métodos HTTP.

### Autenticação

Especializando "ActiveRest::Authentication", um desenvolvedor pode implementar diferentes tipos de comunicação HTTP. A responsabilidade desta classe é configurar o cabeçalho da requisição HTTP com as variáveis de autenticação requeridas pela API de interesse. A classe "ActiveRest::Authentications::Basic" é fornecida para dar suporte ao formato de autenticação "HTTP Basic".

### Transformação de dados (Parsing)

Esta camada envolve a API "Faraday::Json" que é responsável por transformar os dados do formato JSON para instâncias de objetos em memória e vice-versa. Ao delegar essa responsabilidade para um objeto específico definido por nós, estamos habilitando os desenvolvedores a substituir, conforme sua necessidade o formato JSON ou mesmo a biblioteca "Faraday". A classe principal aqui é "ActiveRest::Parser" e na verdade é bem simples internamente, possuindo apenas algumas linhas de código.

### Camada de Interação com os dados (modelo)

É nesta camada que deve atuar o desenvolvedor/usuário do ActiveRest. O módulo principal é "ActiveRest::Model", que expõe os métodos utilitários definidos nos demais módulos que compõem esta camada:

- **ActiveRest::Model::Field**: Adiciona o helper "field", usado para especificar a existencia de um campo da classe, determinando seu tipo de dado. Esse helper especifica automaticamente os métodos acessores necessários para ler e gravar no campo.

- **ActiveRest::Model::BelongsTo**: Adiciona o helper "belongs_to", usado para especificar o atributo referido como uma única instância da classe de domínio passada como parâmetro.

- **ActiveRest::Model::HasMany**: Adiciona o helper "has_many", que referencia uma outra classe de domínio e estecifica que o atributo que está sendo definido é uma lista de instâncias dessa outra classe.

- **ActiveRest::Model::Error**: Adiciona uma lista de erros e o comportamento necessário para administrá-la. A lista de erros é acessível pelo atributo "errors" e os métodos "add_error", "add_errors" e "clear_errors", usados respectivamente para adicionar um único erro, adicionar uma lista de erros e esvaziar a lista de erros.

- **ActiveRest::Model::Proxy**: Adiciona os comportamentos abstratos necessários ao ciclo de vida de um recurso: "create", "update" e "destroy"; e comportamentos para consulta: "list" e "find". As chamadas a "create", "update" e "destroy" resultam respectivamente em requisições HTTP do tipo "POST", "PUT" e "DELETE". As chamadas a "list" e "find" resultam em requisições do tipo "GET", as na primeira espera-se que o resultado seja uma lista e na segunda, uma única instância.

### Diagramas

![](images/diag-classe-geral.png)

[1]: http://www.javaworld.com/article/2945136/scripting-jvm-languages/the-state-of-ruby-and-rails-opportunities-and-obstacles.html "YEGULALP, Serdar; The state of Ruby and Rails: Opportunities and obstacles; JavaWorld; July 2015"
[2]: http://guides.rubygems.org/patterns/ "rubygems.org; May 2016"