# 2 Proposta de solução do problema

No presente trabalho, construiremos um "framework" que permitirá ao desenvolvedor de software, descrever de forma abstrata uma API de serviços REST existente na web e utilizar seus recursos na forma de instâncias de objetos. Isolaremos o código comum (de comunicação HTTP e tratamento de erros) em classes prontas e que o desenvolvedor não vai precisar reprogramar.

O formato JSON será usado para representar os recursos e a fachada de Active Record para expor métodos para o ciclo de vida de um recurso (inserir, atualizar, remover), além de métodos de consulta que retornem um ou mais itens com base em critérios. Quando uma operação de criação, por exemplo, for acionada pelo código executável, a própria API vai produzir o JSON adequado que expressa o estado atual da instância e realizar uma chamada HTTP para o um endpoint previamente configurado usando o verbo HTTP adequado. Na mão oposta, quando uma consulta for realizada, uma chamada HTTP de consulta a um recurso será realizada e a representação do recurso no formato JSON será transformada de volta em uma instância da classe em questão na memória do processo cliente. Também serão fornecidos pontos de extensão que poderão ser usados para, por exemplo, customizar a forma como é realizada a autenticação no nível do protocolo HTTP.

Para definir um recurso, o desenvolvedor será servido de uma classe básica que representará um recurso genérico e classes utilitárias com métodos construídos com base numa semântica de tipos que relaciona tipos de dados da linguagem com as representações que esses tipos devem ter em um documento JSON. Assim, será possível programar um recurso específico, como por exemplo um 'tweet' da API do Twitter na forma de uma classe 'Tweet' que descende da classe básica e seus atributos serão descritos invocando métodos utilitários específicos para cada tipo de dado.

Escolhemos o formato de software livre para distribuir o produto, visto que o objetivo principal é contribuir com a produtividade de outros desenvolvedores. As regras para tornar o software resultante num produto livre, garantem também que ninguém poderá torná-lo um sistema proprietário.

## 2.2 Adoção do suporte REST/JSON

ReST (abreviação para "Representational State Transfer", ou Transferência de Estado Representacional) é um padrão arquitetural proposto por Roy Fielding [(Fielding 2000)][3] em sua dissertação de doutorado. Faz uso de uma representação de recurso que pode ser livre de esquema, e obedece às especificações do protocolo HTTP 1.1. As representações podem ser escritas em qualquer tipo de mídia textual.

O formato JSON foi inspirado na sintaxe de "Object Initializer" conforme a terceira edição da especificação da ECMA-262 [(ECMA 1999)][1]. É formalmente definido na espeficicação ECMA-404 [(ECMA 2013)][2] e constitui-se de um formato leve de transporte de dados para troca de informações projetado para ser usado por qualquer linguagem. E de fato um grande número de linguagens no mercado hoje dá suporte ao uso de JSON. Podemos citar Java, C#, Python e Ruby como exemplos.

Esta conjuntura estimulou a adoção da combinação ReST/JSON por grandes nomes da internet como por exemplo Amazon, Google e Instagram [(Mason 2011)][4], influenciando a decisão de empresas menores na construção de suas API's. Ainda de acordo com Mason, XML está perdendo terreno para JSON. Nestes termos concluímos que a estratégia mais favorável para construção de um produto de utilidade para a comunidade de desenvolvedores deveria ter como alvo, as API's que empregam ReST com JSON.

## 2.3 Definição da Semântica de tipos

Conforme a especificação ECMA-404, um objeto é composto de uma lista de pares nome/valor, sendo que os nomes são formados por sequencias de caracteres e os valores estruturados como objetos, arranjos, seqüências de caracteres, números ou os literais 'true', 'false' e 'null'. No presente trabalho, é introduzida uma semântica de tipos, conforme tabela abaixo, para ser usada no processo de especificação das estruturas que formam as abstrações dos recursos. Os nomes 'object', 'array', 'number' e 'string' têm sua estrutura especificada na própira ECMA-404.

| Tipo        | Valores na ECMA-404                   |
|-------------|---------------------------------------|
| String      | string ou 'null'                      |
| Number      | number ou 'null'                      |
| Boolean     | 'true', 'false' ou 'null'             |
| Array       | array ou 'null'                       |
| Association | object ou 'null'                      |

O tipo 'Association' refere-se à uma subestrutura ou sub recurso. Algumas implementações implementações ReST (como é o caso do formato HAL) podem usar vínculos para referenciar esses subrecursos, requerendo que o cliente faça mais um acesso de rede para completar o objeto pai. No presente trabalho, em função das restrições de tempo e da complexidade de projeto requerida para dar suporte a esse tipo de vinculação, decidimos dar suporte somente aos subrecursos que são fornecidos de forma embarcada no recurso pai.

## 2.3 Sobre a adoção do Active Record

Constitui-se num padrão arquitetural usado na construção de aplicações corporativas, que envolve uma linha de tabela ou visão de uma base de dados, encapsulando o acesso a dados e adicionando-lhe lógica de domínio [(Fowler 2003)][5]. Fowler argumenta que a essência de um "Active Record" é um modelo de domínio no qual as classes se assemelhem ao máximo às estruturas na base de dados e cada uma é responsável por carregar e salvar os dados na base, além de executar lógica de negócios. Cada coluna numa tabela ou view deve corresponder a um campo de mesmo tipo na classe de domínio que a representa.

Com relação ao uso, Fowler também argumenta que esse padrão é indicado para situações nas quais a lógica de domínio não é muito complexa como leituras, atualizações, exclusões e criações de entidades. O presente projeto trata justamente desse conjunto de operações, mas em relação à recursos REST.

No tocante aos relacionamentos entre entidades, as implementações de Active Record podem simplesmente trazer as chaves estrangeiras como campos componentes da classe de domínio, que torna a construção das API's menos custosa. Neste ponto nossa implementação diverge, pois em função de restrições de tempo de desenvolvimento e da natureza dos recursos REST, a opção de menor custo (em termos de tempo de desenvolvimento) é assumir que as possíveis associações serão carregadas a partir de objetos embarcados na forma de alguma propriedade de uma entidade pai.

## 2.5 Esquema de autenticação

É pratica comum na construção de API's, o estabelecimento de algum tipo de sistema de identificação de usuário para fins de controle de acesso e ou rastreamento de operaçoes e suporte. Num momento prévio às chamadas HTTP, o sistema cliente deve carregar adequadamente as credenciais de acesso e configurar a requisição HTTP com estas informações. O projeto corrente dá suporte a essa etapa na forma de um mecanismo de extensão, que permite a definição de um comportamento específico para cada API.

## 2.4 Como adquirir a biblioteca para uso em seu projeto

Software livre habilita profissionais e entuziastas a contribuir com o desenvolvimento e continuidade de um produto. Conforme descrito nos procedimentos da "Free Software Foundation", existe um conjunto de requerimentos específicos que devem ser atendidos para que o software seja considerado livre. Estimamos que o atendimento de todos os requisitos seja um processo longo que se estenderá para além do tempo disponível para a conclusão deste Trabalho de Conclusão de Curso.

O sistema pode ser baixado a partir do seu repositório público no Github a partir do endereço "https://github.com/allanbatista/active-rest". Git tem um sistema bastante vantajoso de commits locais e replicação controlada, que facilita a coordenação do trabalho de desenvolvimento. Github é um repositório gratuito e amplamente difundido.

[1]: http://www.ecma-international.org/publications/files/ECMA-STARCH/ECMA-262,%203rd%20edition,%20December%201999.pdf "ECMA INTERNATIONAL; Standard ECMA-262 3rd Edition; December 1999; p41"
[2]: http://www.ecma-international.org/publications/standards/Ecma-404.htm "ECMA INTERNATIONAL; Standard ECMA-404 1st Edition; October 2013"
[3]: http://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm "FIELDDING, Thomas Roy; Architectural Styles and the Design of Network-based Software Architectures; Chapter 5; Irvine; University of California; 2000"
[4]: https://www.infoq.com/articles/rest-soap "MASON, Ross; How REST replaced SOAP on the Web: What it means to you; InfoQ October 2011"
[5]: http://martinfowler.com "FOWLER, Martin; Patterns of Enterprise Application Architecture; p160; Pearson Education, Inc.; 2003"